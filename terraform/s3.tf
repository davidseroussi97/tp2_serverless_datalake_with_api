# Créer un bucket aws_s3_bucket

resource "aws_s3_bucket" "b" {
  bucket        = "s3-job-offer-bucket-seroussi-lehenaff"
  acl           = "private"
  force_destroy = true
}

# Créer un répertoire aws_s3_bucket_object avec job_offers/raw/

resource "aws_s3_bucket_object" "job_offers" {
  bucket = "${aws_s3_bucket.b.id}"
  acl    = "private"
  key    = "job_offers/raw/"
  source = "/dev/null"
}

# Créer un event aws_s3_bucket_notification pour trigger la lambda

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${aws_s3_bucket.b.id}"

  lambda_function {
    lambda_function_arn = "${aws_lambda_function.data_processing.arn}"
    events              = ["s3:ObjectCreated:Put"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }
}